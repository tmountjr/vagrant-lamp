# Vagrant LAMP setup
This project is designed as a way to seamlessly set up a local development server, and a separate local database server using Vagrant and Puppet.

## Getting Started
After cloning the repo, copy the `vagrant_config.json.example` file to `vagrant_config.json`. Edit `vagrant_config.json` file and fill in the values requested in the file. Not all environment variables are required; the comments will tell you if a value is required for a specific setting, and if the setting is optional, the comment will tell you the default value.

Finally, execute `vagrant up` to load both instances. If you only wish to load a new dev server, execute `vagrant up devserver`; if you only wish to load a new database server, execute `vagrant up dbserver`.

You'll need to specify which environment to connect to when using ssh. Use `vagrant ssh devserver` to ssh into the dev server and `vagrant ssh dbserver` to ssh into the db server.

## Modifying the Options
All the options for both the `devserver` and `dbserver` virtual environments are located in the `vagrant_config.json` file. The `global` key defines default values that are used for both environments and which can, in some instances, be overridden (see below); values that are specific to a specific environment are found in those keys.

Key                                          | Description
---------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
`global.default_box`                         | Required. The default box to use when provisioning the servers. Override within a role using the `box` key.
`global.default_memory`                      | Required. The default amount of memory to use when provisioning the servers. Override within a role using the `memory` key.
`global.mysql_root_password`                 | Required. The password to use for the MySql root user.
`global.dns_base`                            | Required. The domain to use when setting up virtualhosts. Include the leading `.`.
`global.mapped_folders`                      | Optional. The folders to map to all virtual guests. Use absolute paths for the `remote` value to map the folder to the guest `/` folder; otherwise, folders will be mapped to `/shared/`.
`global.nfs`                                 | Required. Boolean to control whether NFS is used to handle synchronized folders (`true`) or VirtualBox Synced Folders are used (`false`).
`roles.{devserver,dbserver}.box`             | Optional. The name of the box to use for the guest. Overrides `global.default_box`.
`roles.{devserver,dbserver}.memory`          | Optional. The amount of memory, in MB, for the guest. Vagrant recommends using 1/4 of the installed system memory. Overrides `global.default_memory`.
`roles.{devserver,dbserver}.ip`              | Optional. The IP address for the guest. If none is specified, the IP will be assigned via DHCP.
`roles.{devserver,dbserver}.mapped_folders`  | Optional. The folders to map to this virtual guest. Use absolute paths for the `remote` value to map the folder to the guest `/` folder; otherwise, folders will be mapped to `/shared/`.
`roles.devserver.mapped_sites`               | Optional. An object with folders that should be mapped to the guest's `/var/www` directory. Only useful for `devserver`. Further explanation available in `vagrant_config.json.example`.
`roles.dbserver.include_mysql_root_password` | Optional. Include the mysql root password (`global.mysql_root_password`) as a fact for Puppet. Only useful for `dbserver`, where it should be `true`. If `false`, you'll need to set up a password on the `dbserver` manually.
`roles.dbserver.set_mysql_bind_address`      | Optional. Set the `bind_address` value in the server's `/etc/mysql/my.cnf` to be bound to the Vagrant IP address if `true`. If `false`, the default value of `127.0.0.1` will be used and the MySQL server will not be available directly from within the private network environment (ie. to use MySQL tools you'll need to use an SSH tunnel). Only useful for `dbserver`.

## Note on Multisites

Starting with `v2.0.1`, this project has the ability to host Drupal multisites. Check the `vagrant_config.json.example` file for more details.
