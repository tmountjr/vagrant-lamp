(function($) {

    $(document).ready(function() {

        $.get('js/known_sites.json')
            .then(function(data) {
                var source = $("#known-sites-template").html(),
                    template = Handlebars.compile(source),
                    html = template({
                        sites: data
                    });

                $(".known-sites").html(html).fadeIn();
            });
    });

})(jQuery);
