# set up the mysql server
class tmountjr::mysql::server (
    $bind_address = 'localhost'
) {

    # specify additional options for the /etc/mysql/my.cnf file
    $_override_options = {
        'mysqld' => {
            # Ensure that the service is bound to the address passed to the class
            # so other systems in the private network can communicate with this
            # service. If no IP was passed, bind to localhost, which will not
            # accept connections from other systems on the network.
            'bind-address' => $bind_address
        }
    }

    class { '::mysql::server':
        root_password    => $::mysql_root_password,
        restart          => true,
        override_options => $_override_options,
    }

    # Ensure that the root user can connect directly via IP from the internal
    # private network
    mysql_user { 'root@10.%':
        ensure        => 'present',
        password_hash => mysql_password($::mysql_root_password),
        require       => Class['::mysql::server']
    }

    mysql_grant { 'root@10.%/*.*':
        ensure     => 'present',
        options    => ['GRANT'],
        privileges => ['ALL'],
        table      => '*.*',
        user       => 'root@10.%',
        require    => Mysql_User['root@10.%']
    }

}
