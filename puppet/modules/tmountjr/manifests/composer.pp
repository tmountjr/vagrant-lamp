# Class: tmountjr::composer
#
# Manages the Composer installation
#
# Parameters:
#  - php_version: the php version to use.
#
# Actions:
#  - Install Composer
#
# Requires:
#  - willdurand-composer
#
# Sample Usage:
#
#     tmountjr::composer {
#         php_version => "5.6"
#     }
class tmountjr::composer (
    $php_version = undef
) {
    class { '::composer':
        target_dir  => '/usr/local/bin',
        auto_update => true,
        require     => Package["php${php_version}", "php${php_version}-mcrypt"]
    }
}
