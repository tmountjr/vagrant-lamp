# Class: tmountjr::drush
#
# Manages the Drush installation
#
# Parameters:
#
# Actions:
#  - Install Drush
#
# Requires:
#  - previousnext-drush
#  - willdurand-composer
#
# Sample Usage:
#
#     include ::tmountjr::drush
class tmountjr::drush {

    drush::drush { 'drush':
        version => 8
    }

    file { "/home/${::home_user}/.drush":
        ensure => directory,
        owner  => $::home_user,
        group  => $::home_user
    }

    file { "/home/${::home_user}/.drush/drushrc.php":
        ensure => present,
        source => 'puppet:///modules/tmountjr/drush/drushrc.php',
        owner  => $::home_user,
        group  => $::home_user
    }

}
