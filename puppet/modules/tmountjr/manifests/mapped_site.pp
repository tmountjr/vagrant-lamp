# Defined Type: tmountjr::mapped_site
#
# Defines a repeatable resource to configure a mapped vhost from the host
define tmountjr::mapped_site (
    $vhost_name,
    $aliases = []
) {

    $_docroot = "/var/www/${vhost_name}"
    $_fqdn = "${vhost_name}${::dns_base}"

    ::apache::vhost { "http_${_fqdn}":
        servername    => $_fqdn,
        port          => '80',
        docroot       => $_docroot,
        directories   => [{
            'path'           => $_docroot,
            'options'        => ['Indexes', 'FollowSymLinks'],
            'allow_override' => ['All']
        }],
        serveraliases => $aliases,

        # soon...once we know ssl is working everywhere
        # redirect_status => 'permanent',
        # redirect_dest   => "https://${_fqdn}",
    }

    ::apache::vhost { "https_${_fqdn}":
        servername    => $_fqdn,
        port          => '443',
        docroot       => $_docroot,
        ssl           => true,
        serveraliases => $aliases,

        # soon...
        # ssl_cert    => "/etc/letsencrypt/live/${_fqdn}/cert.pem",
        # ssl_key     => "/etc/letsencrypt/live/${_fqdn}/privkey.pem",
        # ssl_chain   => "/etc/letsencrypt/live/${_fqdn}/fullchain.pem",

        # but for now, use apache's default self-signed jawn
        ssl_cert      => '/etc/ssl/certs/ssl-cert-snakeoil.pem',
        ssl_key       => '/etc/ssl/private/ssl-cert-snakeoil.key',
        directories   => [{
            'path'           => $_docroot,
            'options'        => ['Indexes', 'FollowSymLinks'],
            'allow_override' => ['All']
        }],
    }

    host { "host_${vhost_name}":
        ensure => present,
        name   => $_fqdn,
        ip     => '127.0.0.1',
    }

}
