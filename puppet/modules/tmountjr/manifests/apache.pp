# set up the default apache instance
class tmountjr::apache (
    # vars that already have defaults from the tmountjr::apache::params class
    $default_webroot = $tmountjr::apache::params::default_webroot
) inherits tmountjr::apache::params {
    class { '::apache':
        default_vhost => false,
        purge_configs => true,

        # set this to false because while normally Ubuntu uses the worker mod,
        # php5.6 requires the prefork mod, and both can't run simultaneously.
        mpm_module    => false,
    }
    apache::listen { [
        '80',
        '443'
    ]: }

    file { $default_webroot:
        ensure => 'directory',
        owner  => 'root',
        group  => 'root'
    }

    file { [
        "${default_webroot}/html"
    ]:
        ensure  => absent,
        force   => true,
        require => Class['::apache']
    }

    class { [
        '::apache::mod::rewrite',
        '::apache::mod::proxy',
        '::apache::mod::prefork',
        '::apache::mod::cgi',
        '::apache::mod::cgid',
        '::apache::mod::fastcgi',
        '::apache::mod::actions'
    ]: }

    ::apache::mod { 'proxy_http': }

    # create fcgi handlers for all supported PHP versions and the necessary config to handle them
    file { "${default_webroot}/cgi-bin":
        ensure => directory
    }

    ['5.6', '7.0', '7.1', '7.2'].each | $php_version | {
        file { "${default_webroot}/cgi-bin/php${php_version}.fcgi":
            ensure  => present,
            content => epp('tmountjr/fcgi.epp', { version => $php_version }),
            owner   => 'root',
            group   => 'root',
            mode    => 'a+x',
        }
    }

    ::apache::custom_config { 'multi_php':
        content => epp('tmountjr/multi_php.conf.epp', { webroot => $default_webroot })
    }

    # ensure any vhost redirects /robots.txt to a global one that blocks
    # all crawlers
    class { 'global_robots': }

}
