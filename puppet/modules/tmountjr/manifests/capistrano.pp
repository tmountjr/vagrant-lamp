# Class: tmountjr::capistrano
#
# Creates a suitable capistrano target environment_path
#
# Parameters:
#
# Actions:
#  - creates a filesystem and database
#  - sets SSH options to allow for remote login and code deploy
#
# Requires:
#
# Sample Usage:
#
class tmountjr::capistrano (
    # vars that must be passed
    $username,
    $password_hash,

    # vars that can be omitted and will be handled
    $groupname                  = undef,
    $uid                        = undef,
    $gid                        = undef,
    $additional_authorized_keys = {},

    # vars that already have defaults from the tmountjr::capistrano::params class
    $default_authorized_keys = $tmountjr::capistrano::params::authorized_keys
) inherits tmountjr::capistrano::params {

    $_groupname = $groupname ? {
        undef   => $username,
        default => $groupname
    }

    $_all_users = parsejson($::all_users)
    $_username_exists = has_key($_all_users, $username)

    if !$_username_exists {
        $_uid = $uid ? {
            undef   => $::next_uid,
            default => $uid
        }

        $_gid = $gid ? {
            undef   => $::next_gid,
            default => $gid
        }
    } else {
        # $username is a local user already; if $uid and $gid are undef, set
        # $_uid and $_gid to the existing uid and gid, otherwise set to $uid
        # and $gid

        $_username_uid = $_all_users[$username]['uid']
        $_username_gid = $_all_users[$username]['gid']

        $_uid = $uid ? {
            undef   => $_username_uid,
            default => $uid
        }

        $_gid = $gid ? {
            undef   => $_username_gid,
            default => $gid
        }
    }

    if $password_hash !~ /^\$1\$.{31}$/ {
        fail('Password must be a hash generated with "openssl passwd -1".')
    }

    ::tmountjr::config::user { "user-${username}":
        username       => $username,
        groupname      => $_groupname,
        groups         => [$::apache::params::group],
        uid            => $_uid,
        gid            => $_gid,
        password       => $password_hash,
        purge_ssh_keys => true,
        require        => Class['::apache']
    }

    $_final_authorized_keys = merge($default_authorized_keys, $additional_authorized_keys)
    $_final_authorized_keys.each | $key_user, $hash | {
        ssh_authorized_key { "authorized-key-${key_user}":
            user    => $username,
            type    => 'ssh-rsa',
            key     => $hash,
            require => Tmountjr::Config::User["user-${username}"]
        }
    }

    sshkey { "known-host-${username}":
        ensure  => 'present',
        name    => 'bitbucket.org',
        key     => 'AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==',
        type    => 'ssh-rsa',
        require => Tmountjr::Config::User["user-${username}"]
    }

}