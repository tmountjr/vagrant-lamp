# Class: tmountjr::pip
#
# Manages the Pip installation
#
# Parameters:
#  - packages: an array of packages to install
#
# Requires:
#
# Sample Usage:
#
#     class { 'tmountjr::pip':
#         packages => [
#             'Jinja2'
#         ]
#     }
class tmountjr::pip (
    $packages = []
) {
    package { 'python-pip':
        ensure   => present,
        provider => 'apt',
    }

    package { $packages:
        ensure   => present,
        provider => 'pip',
        require  => Package['python-pip']
    }
}
