# Defined Type: tmountjr::config::user
#
# Defines a repeatable resource to configure a new user
define tmountjr::config::user (
    $username,
    $uid            = undef,
    $gid            = undef,
    $groupname      = undef,
    $groups         = [],
    $password       = '*',
    $key            = undef,
    $purge_ssh_keys = false
) {

    $_groupname = $groupname ? {
        undef   => $username,
        default => $groupname
    }

    $_uid_hash = $uid ? {
        undef   => {},
        default => {'uid' => $uid}
    }
    $_gid_hash = $gid ? {
        undef   => {},
        default => {'gid' => $gid}
    }
    $_uid_gid_hash = merge($_uid_hash, $_gid_hash)

    user { $username:
        ensure         => present,
        *              => $_uid_gid_hash,
        groups         => $groups,
        home           => "/home/${username}",
        password       => $password,
        purge_ssh_keys => $purge_ssh_keys,
        shell          => '/bin/bash',
    }

    group { $_groupname:
        gid => $gid
    }

    file { "/home/${username}":
        ensure  => directory,
        owner   => $username,
        group   => $_groupname,
        mode    => '0750',
        require => [ User[$username], Group[$_groupname] ]
    }

    file { "/home/${username}/.ssh":
        ensure  => directory,
        owner   => $username,
        group   => $_groupname,
        mode    => '0700',
        require => File["/home/${username}"]
    }

    if $key {
        ssh_authorized_key { "${username}-${_groupname}":
            user    => $username,
            type    => 'ssh-rsa',
            key     => $key,
            require => File["/home/${username}/.ssh"]
        }
    }
}
