# Resource: tmountjr::letsencrypt
#
# Configure the let's encrypt cert automatically
#
# Parameters:
#  - fqdn: The fully-qualified domain name of the site
#  - email: The email to use when registering the Let's Encrypt certificate
#
# Actions:
#  - Get a Let's Encrypt certificate
#
# Requires:
#
# Sample Usage:
#
#    tmountjr::letsencrypt { 'le_mysite':
#        fqdn  => 'mysite.domain.com',
#        email => 'notifications@domain.com'
#    }
define tmountjr::letsencrypt (
    $fqdn,
    $email = 'admin@example.com'
) {

    apt::ppa{'ppa:certbot/certbot': } -> Exec['apt-update-single']
    package { 'python-certbot-apache':
        ensure  => present,
        require => Apt::Ppa['ppa:certbot/certbot'],
    }

    $maincommand = "certbot --text --agree-tos -n -m ${email} certonly --apache -d ${fqdn}"
    $croncommand = "${maincommand} --keep-until-expiring > /dev/null 2>&1"

    exec { "letsencrypt ${fqdn}":
        command => $maincommand,
        path    => '/usr/bin',
        creates => "/etc/letsencrypt/live/${fqdn}/cert.pem",
        require => Package['python-certbot-apache'],
    }

    $cron_hour = fqdn_rand(24, $fqdn)
    $cron_minute = fqdn_rand(60, $fqdn)
    file { "${::puppet_vardir}/letsencrypt":
        ensure => 'directory',
        owner  => 'root',
        group  => 'root',
    }
    file { "${::puppet_vardir}/letsencrypt/renew-${fqdn}.sh":
        ensure  => 'file',
        mode    => '0755',
        owner   => 'root',
        group   => 'root',
        content => "#!/bin/sh\n${croncommand}",
    }
    cron { "letsencrypt renew cron ${fqdn}":
        command => "${::puppet_vardir}/letsencrypt/renew-${fqdn}.sh",
        user    => 'root',
        hour    => $cron_hour,
        minute  => $cron_minute,
    }

}