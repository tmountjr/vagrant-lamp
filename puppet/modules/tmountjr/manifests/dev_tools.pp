# Class: tmountjr::dev_tools
#
# Installs the dev tools necessary to compile websites
#
# Parameters:
#
# Actions:
#  - Installs the Grunt CLI globally
#  - Installs the Gulp CLI globally
#  - Installs fontforge and ttfautohint
#  - Installs the Drupal Console Launcher
#
# Requires:
#  - tmountjr::node
#
# Sample Usage:
#
#     include tmountjr::dev_tools
class tmountjr::dev_tools {

    include tmountjr::node

    package { [
        'grunt-cli',
        'gulp-cli'
    ]:
        ensure   => present,
        provider => npm,
    }

    # Used when converting svg glyphs to fonts
    package { [
        'fontforge',
        'ttfautohint',
        'multitail'
    ]:
        ensure   => present,
        provider => apt,
    }

    # Install Drupal Console Launcher
    exec { 'download_drupal_console':
        command => 'curl https://drupalconsole.com/installer -L -o /usr/local/bin/drupal',
        creates => '/usr/local/bin/drupal',
        path    => ['/usr/bin', '/usr/sbin/']
    }
    file { '/usr/local/bin/drupal':
        mode    => '0755',
        require => Exec['download_drupal_console']
    }

}
