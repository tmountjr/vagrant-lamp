# Class: tmountjr::ruby
#
# Manages the Ruby installation
#
# Parameters:
#
# Actions:
#  - Install ruby
#  - Install ruby dev (including bundler and gem)
#
# Requires:
#  - puppetlabs-ruby
#
# Sample Usage:
#
#     include tmountjr::ruby
class tmountjr::ruby {
    class { '::ruby':
        gems_version => 'latest'
    }

    include ::ruby::dev
}
