# Defined Type: tmountjr::capistrano::target
#
# Defines a repeatable resource to configure a new deploy target
define tmountjr::capistrano::target (
    $siteroot,
    $username,
    $groupname,
    $dbname,
    $dbusername,
    $dbpassword,
    $docroot = $::tmountjr::capistrano::params::docroot,
    $dbhost = $::tmountjr::capistrano::params::dbhost,
    $dbgrant = $::tmountjr::capistrano::params::dbgrant,
) {

    $_webroot = "${docroot}/${siteroot}"

    file { $_webroot:
        ensure  => directory,
        owner   => $::apache::params::user,
        group   => $groupname,
        purge   => false,
        mode    => '0775',
        require => Class['::apache']
    }

    file { [
        "${_webroot}/shared",
        "${_webroot}/shared/files",
    ]:
        ensure  => directory,
        owner   => $::apache::params::user,
        group   => $::apache::params::group,
        recurse => true,
        purge   => false,
        require => File[$_webroot]
    }

    file { "${_webroot}/shared/settings.php":
        ensure  => file,
        owner   => $::apache::params::user,
        group   => $::apache::params::group,
        purge   => false,
        mode    => 'a=r',
        require => File[$_webroot]
    }

    file { "${_webroot}/releases":
        ensure  => directory,
        owner   => $username,
        group   => $groupname,
        purge   => false,
        require => [ File[$_webroot], Tmountjr::Config::User["user-${username}"] ]
    }

    mysql::db { "db-${dbname}":
        dbname   => $dbname,
        user     => $dbusername,
        password => $dbpassword,
        host     => $dbhost,
        grant    => $dbgrant,
        require  => Class['Tmountjr::Mysql::Server']
    }
}