# Class: tmountjr::capistrano_params
#
# Initializes the default parameters for the capistrano target
class tmountjr::capistrano::params {
    $authorized_keys = {}
    $docroot         = '/var/www'
    $dbhost          = 'localhost'
    $dbgrant         = ['ALL']
}
