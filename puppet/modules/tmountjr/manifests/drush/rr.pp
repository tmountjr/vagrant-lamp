# Class: tmountjr::drush::rr
#
# Installs Drush Registry Rebuild (Drupal 7.x)
#
# Actions:
# - Installs drush rr
#
# Sample Usage:
#  include ::tmountjr::drush::rr
class tmountjr::drush::rr {

    exec { 'install_drush_registry_rebuild':
        command => 'drush @none dl registry_rebuild-7.x',
        creates => "/home/${::home_user}/.drush/registry_rebuild.drush.inc",
        path    => ['/bin', '/usr/bin', '/usr/local/bin'],
        returns => ['', 0],
        require => Class['tmountjr::Drush']
    }

}