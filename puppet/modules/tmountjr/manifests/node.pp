# Class: tmountjr::node
#
# Manages the Node + NPM installation
#
# Parameters:
#
# Actions:
#  - Installs Node.js
#  - Installs NPM
#
# Requires:
# - puppet-nodejs
#
# Sample Usage:
#
#     include tmountjr::node
class tmountjr::node {
    class { '::nodejs':
        manage_package_repo       => false,
        nodejs_dev_package_ensure => 'present',
        npm_package_ensure        => 'present',
    }
}
