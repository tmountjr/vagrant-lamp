# Make sure the site is protected by basic auth
define tmountjr::apache::basic_auth (
    $username,
    $password
) {
    $htpasswd_path = "${::apache::params::httpd_dir}/.htpasswd"

    # Set up the username in .htpasswd
    httpauth { $username:
        ensure    => present,
        file      => $htpasswd_path,
        password  => $password,
        mechanism => basic
    }

    # Fix permissions
    file { "${htpasswd_path}_mode":
        path    => $htpasswd_path,
        mode    => '0644',
        require => Httpauth[$username]
    }

    # Copy down the config file
    ::apache::custom_config { 'basic_auth':
        content  => epp('es/basic_auth.conf.epp', {
            path => $htpasswd_path
        }),
        priority => '20',
        require  => Httpauth[$username]
    }
}