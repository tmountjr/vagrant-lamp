# Class: tmountjr::apache::params
#
# Initializes the default parameters for the tmountjr::Apache class
class tmountjr::apache::params {
    # for readbility's sake when this is used, omit the trailing slash
    # just, when you use it, remember to include the slash in the concatenation.
    $default_webroot = '/var/www'
}