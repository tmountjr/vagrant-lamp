# ensure that there is a global robots.txt applied
class tmountjr::apache::global_robots (
    $filename = 'robots_noindex.txt',
    $path     = $tmountjr::apache::params::default_webroot,
) inherits tmountjr::apache::params {

    file { "${path}/${filename}":
        ensure  => present,
        source  => 'puppet:///modules/tmountjr/apache/global_robots/robots.txt',
        owner   => 'root',
        group   => 'root',
        require => Class['::apache']
    }

    ::apache::custom_config { 'global_robots':
        content => epp('es/global_robots.conf.epp', {
            path     => $path,
            filename => $filename
        })
    }

}