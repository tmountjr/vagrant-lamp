# Defined Type: tmountjr::drupal_site
#
# Defines a repeatable resource to configure a mapped vhost from the host
define tmountjr::drupal_site (
    $vhost_name,
    $php_version,
    $multisites = [],
    $is_deploy_target = false
) {

    $_docroot = $is_deploy_target ? {
        true => "/var/www/${vhost_name}/current",
        default => "/var/www/${vhost_name}"
    }
    $_fqdn = "${vhost_name}${::dns_base}"

    if ($is_deploy_target) {
        $ssl_options = {
            ssl_cert => "/etc/letsencrypt/live/${_fqdn}/cert.pem",
            ssl_key => "/etc/letsencrypt/live/${_fqdn}/privkey.pem",
            ssl_chain => "/etc/letsencrypt/live/${_fqdn}/fullchain.pem",
            ssl_protocol => ['all', '-SSLv3', '-TLSv1', '-TLSv1.1'],
            ssl_cipher => 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256',
            ssl_stapling => true,
            ssl_stapling_timeout => 5,
            ssl_stapling_return_errors => false,
        }
        $redirects = {
            'redirect_status' => 'permanent',
            'redirect_dest' => "https://${_fqdn}"
        }
    } else {
        $ssl_options = {
            ssl_cert => '/etc/ssl/certs/ssl-cert-snakeoil.pem',
            ssl_key => '/etc/ssl/private/ssl-cert-snakeoil.key'
        }
        $redirects = {}
    }

    ::apache::vhost { "http_${_fqdn}":
        servername     => $_fqdn,
        port           => '80',
        docroot        => $_docroot,
        directories    => [{
            path            => $_docroot,
            options         => ['Indexes', 'FollowSymLinks'],
            allow_override  => ['All'],
            addhandlers     => [{
                handler    => 'php-cgi',
                extensions => ['.php']
            }],
            custom_fragment => "Action php-cgi /php-fcgi/php${php_version}.fcgi"
        }],
        serveraliases  => $multisites,

        # soon...once we know ssl is working everywhere
        *              => $redirects,
        manage_docroot => !$is_deploy_target,
    }

    ::apache::vhost { "https_${_fqdn}":
        servername     => $_fqdn,
        port           => '443',
        docroot        => $_docroot,
        directories    => [{
            path            => $_docroot,
            options         => ['Indexes', 'FollowSymLinks'],
            allow_override  => ['All'],
            addhandlers     => [{
                handler    => 'php-cgi',
                extensions => ['.php']
            }],
            custom_fragment => "Action php-cgi /php-fcgi/php${php_version}.fcgi"
        }],
        serveraliases  => $multisites,
        headers        => [
            'always set Strict-Transport-Security "max-age=15768000"'
        ],

        ssl            => true,
        manage_docroot => !$is_deploy_target,
        *              => $ssl_options
    }

    host { "host_${vhost_name}":
        ensure => present,
        name   => $_fqdn,
        ip     => '127.0.0.1',
    }

    file { "/home/${::home_user}/.drush/${vhost_name}.aliases.drushrc.php":
        ensure  => present,
        content => epp('tmountjr/site.aliases.drushrc.php.epp', {
            'vhost_name' => $vhost_name,
            '_fqdn'      => $_fqdn,
            '_docroot'   => $_docroot,
            'multisites' => $multisites
        }),
        owner   => $::home_user,
        group   => $::home_user,
        require => File["/home/${::home_user}/.drush"]
    }

}
