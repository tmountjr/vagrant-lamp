# set up memcached
class tmountjr::memcached (
    $target_version = undef
) {
    class { '::memcached': }

    package { 'php-memcached':
        ensure  => present,
        require => Class['::tmountjr::php']
    } ~> Class['::apache::service']
}