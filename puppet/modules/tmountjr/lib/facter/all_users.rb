require 'facter'
require 'json'
Facter.add('all_users') do
    setcode do
        userList = Facter::Util::Resolution.exec('cut -d: -f1,3,4 /etc/passwd').split("\n")
        users = {}
        userList.each do |user|
            username, uid, gid = user.split(':')
            users[username] = { 'uid' => uid, 'gid' => gid }
        end
        users.to_json
    end
end
