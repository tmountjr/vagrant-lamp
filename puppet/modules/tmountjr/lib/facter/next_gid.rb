require 'facter'
Facter.add('next_gid') do
    setcode do
        Facter::Util::Resolution.exec("getent passwd | awk -F: '($4>600) && ($4<10000) && ($4>maxgid) { maxgid=$4; } END { print maxgid+1; }'")
    end
end
