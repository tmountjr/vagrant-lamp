<?php

// specify sql-related options for all drush commands
$options['structure-tables']['common'] = [
  'cache',
  'cache_*',
  'history',
  'search_*',
  'sessions',
  'watchdog',
  'xmlsitemap',
  'xmlsitemap_*',
];
$options['structure-tables-key'] = 'common';
$options['result-file'] = '/db/@DATABASE_@DATE.sql';
