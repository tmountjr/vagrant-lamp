class profile::base {

    class { '::ntp':
        servers => [
            '0.ubuntu.pool.ntp.org',
            '1.ubuntu.pool.ntp.org',
            '2.ubuntu.pool.ntp.org',
            '3.ubuntu.pool.ntp.org',
        ]
    }

    include apt
    # Define something that runs before every package installation.
    exec { 'apt-update':
        command => '/usr/bin/apt-get update'
    }
    Exec['apt-update'] -> Package <| |>

    # Define something that can be run in isolation.
    exec { 'apt-update-single':
        command => '/usr/bin/apt-get update'
    }

    include git

    package { [
        'zip',
        'unzip'
    ]:
        ensure => latest
    }

    parsejson($::machine_manifest).each |$manifest| {
        host { "sibling_${manifest['hostname']}":
            ensure => present,
            name   => $manifest['hostname'],
            ip     => $manifest['ip'],
        }
    }
}
