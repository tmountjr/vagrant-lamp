# This class defines a common configuration for all web servers, no matter what its role.
class profile::www {

    # Specify the default webroot
    $default_webroot = '/var/www'

    # Everyone gets the lamp stack
    class { '::tmountjr::apache':
        default_webroot => $default_webroot
    }
    include ::tmountjr::mysql::client
    class { '::tmountjr::php': }
    class { '::tmountjr::memcached': }

    # Everyone gets build tools: composer, ruby, and node
    class { '::tmountjr::composer': }
    include ::tmountjr::ruby
    include ::tmountjr::node

    # Everyone gets status tools: drush
    include ::tmountjr::drush
    include ::tmountjr::drush::rr

    # Everyone gets a custom default vhost
    ::apache::vhost { 'http__default_landing_page':
        servername  => "default_landing_page${::dns_base}",
        port        => '80',
        docroot     => "${default_webroot}/default_landing_page",
        directories => [{
            'path'           => "${default_webroot}/default_landing_page",
            'options'        => ['Indexes', 'FollowSymLinks'],
            'allow_override' => ['All'],
        }],
        priority    => '15',
    }
    ::apache::vhost { 'https__default_landing_page':
        servername  => "default_landing_page${::dns_base}",
        port        => '443',
        docroot     => "${default_webroot}/default_landing_page",
        directories => [{
            'path'           => "${default_webroot}/default_landing_page",
            'options'        => ['Indexes', 'FollowSymLinks'],
            'allow_override' => ['All'],
        }],
        ssl         => true,
        ssl_cert    => '/etc/ssl/certs/ssl-cert-snakeoil.pem',
        ssl_key     => '/etc/ssl/private/ssl-cert-snakeoil.key',
        priority    => '16',
    }

}
