# This class defines things that are specific to dev web servers.
# It includes all the normal stuff you'd find on a web server.
class profile::www::dev inherits profile::www {

    # In addition to the lamp stack and build and status tools, dev servers
    # should get some additional stuff
    class { '::tmountjr::pip':
        packages => ['Jinja2']
    }

    include ::tmountjr::dev_tools

    # Set up mapped sites to have their own vhost entries
    split($::mapped_sites, ',').each |$mapped_site| {
        tmountjr::drupal_site { "drupal_site_${mapped_site}":
            vhost_name  => $mapped_site,
            multisites  => parsejson($::multisites)[$mapped_site],
            php_version => parsejson($::php_versions)[$mapped_site]
        }
    }

    include stdlib

    file_line { 'short_drush_cr':
        line => 'alias dcr="drush cr"',
        path => '/home/ubuntu/.bashrc',
    }

    file_line { 'short_drush_cc_all':
        line => 'alias dcc="drush cc all"',
        path => '/home/ubuntu/.bashrc',
    }

    file_line { 'short_drush_uli':
        line => 'alias duli="drush uli"',
        path => '/home/ubuntu/.bashrc',
    }
}
