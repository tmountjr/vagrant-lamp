# This class defines this that are specific to a capistrano target server.
# It includes all the normal stuff you'd find on a web server.
class profile::www::deploy_target inherits profile::www {

    class { '::tmountjr::mysql::server': }

    class { '::tmountjr::capistrano':
        username      => $::home_user,
        password_hash => '$1$.wWnsXIh$CDuK1R/IriCpHDtonYYNu1',  # "test"
    }

    split($::mapped_sites, ',').each |$mapped_site| {
        tmountjr::capistrano::target { "deploy_target_${mapped_site}":
            siteroot   => $mapped_site,

            # Currently this is being passed in from the Vagrantfile.
            dbname     => $::dbname,
            dbusername => $::dbuser,
            dbpassword => $::dbpass,

            username   => $::home_user,
            groupname  => $::home_user,

            docroot    => '/var/www',
        }

        tmountjr::drupal_site { "deploy_site_${mapped_site}":
            vhost_name       => $mapped_site,
            is_deploy_target => true,
            php_version      => $::php_version,
            require          => tmountjr::Capistrano::Target["deploy_target_${mapped_site}"]
        }

        $_fqdn = "${mapped_site}${::dns_base}"
        tmountjr::letsencrypt{ "le_${_fqdn}":
            fqdn    => $_fqdn,
            require => tmountjr::Drupal_site["deploy_site_${mapped_site}"]
        }

        tmountjr::apache::basic_auth { "basic_auth_${::auth_username}":
            username => $::auth_username,
            password => $::auth_password
        }
    }
}
