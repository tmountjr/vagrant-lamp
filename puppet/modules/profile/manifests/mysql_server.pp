# This class defines a common configuration for all db servers.
class profile::mysql_server {

    class { '::tmountjr::mysql::server':
        bind_address => $::mysql_bind_address
    }
}
