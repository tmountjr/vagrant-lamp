# -*- mode: ruby -*-
# vi: set ft=ruby :

# ensure the required plugin(s) are installed
required_plugins = %w(vagrant-vbguest vagrant-bindfs)
required_plugins.each do |plugin|
    next if Vagrant.has_plugin? plugin || ARGV.include?('plugin')
    system "vagrant plugin install #{plugin}" || abort("Installation of '#{plugin}' has failed. Aborting.")

    exit system('vagrant', *ARGV)
end

# ensure the config file exists
require 'json'
if File.exist?(File.expand_path('./vagrant_config.json'))
    settings = JSON.parse(File.read(File.expand_path('./vagrant_config.json')))
else
    abort 'Settings file (vagrant_config.json) is not readable or does not exist.'
end

# set up a global manifest of machines and their IPs so each node knows the hostname of the others
$machine_manifest = []
settings['roles'].each do |role|
    role_name, role_settings = role
    chosen_hostname = role_settings.key?('hostname') ? role_settings['hostname'] : role_name
    $machine_manifest.push('hostname' => chosen_hostname,
                           'ip'       => role_settings['ip'])
end

# Set up the lambda to mount a folder to an instance
def mount_folder(instance, local, remote, use_nfs)
    # only use a relative path if the "remote" value isn't already absolute
    remote = "/shared/#{remote}" if remote !~ /^\//

    # map the folder.
    if use_nfs === true
        bind_remote = "/var/nfs#{remote}"
        if remote =~ /\/var\/www/
            bind_group = 'www-data'
        else
            bind_group = 'ubuntu'
        end

        instance.bindfs.bind_folder "/var/nfs#{remote}", remote,
            u: 'ubuntu',
            g: bind_group,
            perms: 'ug=rwD:o=rD',
            o: 'nonempty'
        if (/darwin/ =~ RUBY_PLATFORM) != nil
            instance.vm.synced_folder local, bind_remote, nfs: {
                mount_options: ['actimeo=1']
            }, :bsd__nfs_options => ["-maproot=0:0"]
        else
            instance.vm.synced_folder local, bind_remote, nfs: {
                mount_options: ['actimeo=1']
            }, :linux__nfs_options => ["no_root_squash"]
        end
    else
        instance.vm.synced_folder local, remote, mount_options: ['dmode=775,fmode=664'], owner: 'ubuntu', group: 'ubuntu'    
    end    
end

# Set up the provisioner lambda
def provisioner(config, global_settings, role_info)
    role_name, role_settings = role_info
    chosen_hostname = role_settings.key?('hostname') ? role_settings['hostname'] : role_name

    # Make sure the uid/gid used for nfs mounts are accounted for
    config.nfs.map_uid = Process.uid
    config.nfs.map_gid = Process.gid

    config.vm.define role_name do |instance|
        instance.vm.box = role_settings.key?('box') ? role_settings['box'] : global_settings['default_box']
        if role_settings.key?('ip')
            instance.vm.network :private_network, ip: role_settings['ip']
        else
            instance.vm.network :private_network, type: 'dhcp'
        end

        instance.vm.hostname = chosen_hostname + global_settings['dns_base']
        instance.vm.provider :virtualbox do |vb|
            vb.memory = role_settings.key?('memory') ? role_settings['memory'] : global_settings['default_memory']
            vb.cpus = 1
            vb.customize ['modifyvm', :id, '--ioapic', 'on']
        end

        mount_folder(instance, './db', '/db', global_settings['nfs'])

        # map any additional globally-mapped folders to the guest's "/shared" folder
        global_settings['mapped_folders'].each { |mapped_folder| mount_folder(instance, mapped_folder['local'], mapped_folder['remote'], global_settings['nfs']) } if global_settings.key?('mapped_folders')

        instance.vm.provision :puppet do |puppet|
            puppet.manifests_path = 'puppet/manifests'
            puppet.manifest_file = "#{role_name}.pp"
            puppet.module_path = 'puppet/modules'
            puppet.hiera_config_path = 'puppet/hiera.yaml'
            puppet.options = ['--verbose']

            # optionally expose some facts based on the config passed in
            facts = {
                'machine_manifest' => $machine_manifest.to_json,
                'dns_base' => global_settings['dns_base'],
                'home_user' => 'ubuntu'
            }
            mapped_sites = []
            multisites = {}
            mapped_sites_with_aliases = []
            mapped_sites_with_php_verisons = {}

            facts['mysql_root_password'] = global_settings['mysql_root_password'] if role_settings.key?('include_mysql_root_password') && role_settings['include_mysql_root_password']
            facts['mysql_bind_address'] = role_settings['ip'] if role_settings.key?('set_mysql_bind_address') && role_settings['set_mysql_bind_address']
            
            if role_settings.key?('mapped_sites')
                role_settings['mapped_sites'].each do |site|
                    local = site['local']
                    remote = site['remote_vhost'] ||= local.split('/').last
                    mount_folder(instance, local, "/var/www/#{remote}", global_settings['nfs']) if role_name == 'devserver'
                    mapped_sites << remote
                    mapped_sites_with_aliases << remote + global_settings['dns_base']
                    mapped_sites_with_php_verisons[remote] = site['php_version'] ||= "5.6"

                    # ensure a multisite manifest is sent for all sites
                    multisites[remote] = []
                    next unless site.key?('multisites')
                    site['multisites'].each do |ms|
                        multisites[remote] << ms + global_settings['dns_base']
                        mapped_sites_with_aliases << ms + global_settings['dns_base']
                    end
                end
                facts['mapped_sites'] = mapped_sites.join(',')
                facts['multisites'] = multisites.to_json
                facts['php_versions'] = mapped_sites_with_php_verisons.to_json
            end

            # map any additional individually-mapped folders to the guests' "/shared" folder
            role_settings['mapped_folders'].each { |mapped_folder| mount_folder(instance, mapped_folder['local'], mapped_folder['remote'], global_settings['nfs']) } if role_settings.key?('mapped_folders')

            if role_name == 'devserver'
                File.open('puppet/default_landing_page/js/known_sites.json', mode: 'w') { |f| f << mapped_sites_with_aliases.to_json }
                instance.vm.synced_folder 'puppet/default_landing_page', '/var/www/default_landing_page'
            end

            puppet.facter = facts
        end
    end
end

Vagrant.configure('2') do |config|
    config.ssh.forward_agent = true
    config.vm.provision :shell, inline: 'apt-get update && apt-get -qq -y install puppet bindfs'
    config.vm.provision :shell, inline: 'cp /vagrant/puppet/puppet.conf /etc/puppet'

    settings['roles'].each { |role| provisioner(config, settings['global'], role) }
end
