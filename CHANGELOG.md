# 2.2.2

## New Features

* Add `drush rr` (registry rebuild) by default.
* Install SOAP client for PHP.
* Now supports PHP 7.0 and 7.2.

# 2.2.1

## Bugfixes

* Use [BindFS](http://bindfs.org/) to work around NFS shared permission issues.

## Enhancements

* Add common drush shortcuts to Ubuntu profile. (Thanks Migs!)

## New Features

* Add Drupal Console Launcher to the base install. You will still need to install the Drupal Console as a dependency to your project (see [here]https://docs.drupalconsole.com/en/getting/composer.html) for details on installing Drupal Console using Composer).

# 2.2.0

## Bugfixes

* Ensure default webroot exists on new servers.
* Numerous fixes for `deploy_target`s.

## Enhancements

* Add `multitail` to tools installed on all dev servers. (Thanks Spencer!)

# 2.1.3

## Bugfixes

* Fixed permissions errors on `~/.drush` to ensure it's not created as root.

## New Features

* Add ability to use NFS vs. VirtualBox Synced Folders when setting up instances. See `README.md` for details.

# 2.1.2

## Bugfixes

* Fixed a bug where conf files in `/etc/apache2/conf.d` were being created with duplicate `.conf` extensions.
* Fixed incorrect class documentation for cx_deploy module

## Enhancements

* Using `puppet:` URIs, move things that were being stored as templates (but really weren't) into static files and reference them using the `source` attribute.
* Refactor `tmountjr::apache` class to use a configurable parameter for the webroot (by default, `/var/www`).

## New Features

* Add `tmountjr::apache::global_robots`, a submodule which will alias `/robots.txt` on all vhosts to a static file that disallows crawlers/search site indexers.

# 2.1.1

## Bugfixes

* Fixed a bug where HTTPS vhosts weren't getting the same configuration options to use PHP 5.6 or PHP 7.1

## Deprecations

* The config value for `hostname` on each system is deprecated. It can still be specified, but it's strongly recommended to use the role names instead.

## New Features

* Add the ability to globally or individually map custom shared folders to a guest's `/shared/` directory via the `mapped_folders` config value.

# 2.1.0

## New Features

* Memcached is installed by default. NOTE: Drupal still needs to be configured to use memcached.
* Ability to run both PHP 5.6 and PHP 7.1 sites simultaneously. By default, all sites will run using PHP 5.6. To override this behavior, set the `php_version` value to `7.1` inside the `mapped_sites` key of `vagrant_config.json`. For example:

```json
{
    "roles": {
        "devserver": {
            "mapped_sites": [
                {
                    "local": "/path/to/repo",
                    "php_verison": "7.1"
                }
            ]
        }
    }
}
```

Because this release is a minor version bump, please run `vagrant reload --provision devserver` from the root of your project.

# 2.0.5

## New Features

* Setting up a Drupal site will now include `drush` aliases and pre-configured `sql-dump` options.
* All provisioned systems now include a `/db` directory where `drush sql-dump` files will be put. These dumps will be immediately available to all other configured systems.

## Enhancements

* Optimization for NFS shares.

# v2.0.4

## New Features

* Set up a `deploytarget` server type configured to receive a capistrano deployment.

## Enhancements

* Default shared folder type is now NFS.

## Changes

* Config file now uses `multisites` instead of `aliases`.

# v2.0.3

## New Features

* Install `fontforge` and `ttfautohint` for font glyph creation.

## Enhancements

* Add a default landing page that lists currently-configured vhosts. Once this repo is updated locally, run `vagrant reload devserver` and browse to the IP of your `devserver` to see the changes.

# v2.0.2

## New Features

* Install `gulp-cli` and `grunt-cli` globally for machines with `www::dev` profile.
* Ensure `php5.6-zip` package is installed (required for Drupal sites).
* Added this changelog.

# v2.0.1

## New Features

* Drupal multisite compatibility

# v2.0.0

Major breaking release from `v1.0.0`.

## New Features

* Spins up two servers, patterned after a `devserver` and a `dbserver`.
* Creates shared folders and automatically creates new vhost entries.
* Puppet roles redefined for repeatability.

# v1.0.0

v1.0.0 was the initial stable release. Set up one vagrant guest with a web server and a mysql server. Ability to add shared folders but vhosts had to be set up manually.
