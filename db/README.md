# DB Folder
This folder should be used as a dumping ground for database dumps. This folder is mounted across all Vagrant instances as `/db`.

Some potential uses are:

* Creating a dump from a remote upstream server, them moving the dump to this folder so it can be used in the `devserver` or `dbserver`.
* Sharing a local database dump to the `deploytarget` server.
